# multimodal-search

#### Description
本工程是安装Chinese-clip大模型和milvus向量数据库，并实现文搜图的技术步骤

#### Software Architecture
Software architecture description

#### Installation

1.  运行setup.py进行打包
2.  本工程要配合milvus向量数据库一起使用，创建表的脚本为：create_milvus_collection.py
3.  python gradio_main.py 运行本工程

#### Instructions

1.  本项目主要提供两个功能
2.  输入文字搜索图片
3.  上传图片搜索图片

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
