import cv2
import numpy as np


def preprocess_image_opencv(image_path, target_size=(224, 224)):
    # 读取图像
    image = cv2.imread(image_path)

    # 尺寸调整
    image = cv2.resize(image, target_size)

    # 归一化
    image = image.astype("float") / 255.0

    # 可选：转换为灰度图像
    # gray_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

    # 可选：直方图均衡化
    # equalized_image = cv2.equalizeHist(gray_image)

    return image


# 使用示例
image_path = "data/test1/杯子819K-1280x1707.jpg"
preprocessed_image = preprocess_image_opencv(image_path)
