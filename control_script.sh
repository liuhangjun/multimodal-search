#!/bin/bash

# 设置 Python 脚本的路径
PYTHON_SCRIPT="./mSearch_gradio_main.py"
LOG_FILE="log/logfile.log"

start() {
    echo "[$(date)] Starting Python script..." >> "$LOG_FILE"
    nohup python3 $PYTHON_SCRIPT >> "$LOG_FILE" 2>&1 &
    echo "[$(date)] Python script started." >> "$LOG_FILE"
}

stop() {
    echo "[$(date)] Stopping Python script..." >> "$LOG_FILE"
    pkill -f $PYTHON_SCRIPT
    echo "[$(date)] Python script stopped." >> "$LOG_FILE"
}

restart() {
    stop
    sleep 1
    start
}

case "$1" in
    start)
        start
        ;;
    stop)
        stop
        ;;
    restart)
        restart
        ;;
    *)
        echo "Usage: $0 {start|stop|restart}"
        exit 1
        ;;
esac

exit 0
