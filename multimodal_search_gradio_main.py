from app.gradio.image2image import image2image_gr
from app.gradio.text2image import text2image_gr
from app.gradio.text2image_L2 import text2image_gr_l2
import gradio as gr

import sys
import os


# 获取当前文件的目录
current_dir = os.path.dirname(os.path.abspath(__file__))
# 将app文件的目录添加到 sys.path
sys.path.append(current_dir)

if __name__ == "__main__":
    gr.close_all()
    with gr.TabbedInterface(
            [text2image_gr(), text2image_gr_l2(), image2image_gr()],
            ["文到图搜索Cosine", "文到图搜索L2(Distance)", "图到图搜索Cosine"],
    ) as app:
        app.launch(server_name='0.0.0.0', server_port=8082, share=False)
