from pymilvus import connections


def create_connection(host, port, user, passwd):
    print(f"\nCreate connection...")
    connections.connect(host=host, port=port, user=user, password=passwd, timeout=15)
    print(f"\nList connections:")
    print(connections.list_connections())
    return None
