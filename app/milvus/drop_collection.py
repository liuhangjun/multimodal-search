from pymilvus import MilvusClient


def drop_collection(collection_name):
    milvus_client = MilvusClient("http://localhost:19530")

    print(f"Start to drop collection {collection_name}")

    try:
        # drop collection
        milvus_client.drop_collection(collection_name)
        print("Drop successful.")
    except Exception as e:
        print(f"Failed to Drop collection: {e}")


if __name__ == "__main__":
    # multimodal_search_H14_L2  multimodal_search_H14  multimodal_search_L14
    collection_name = "multimodal_search_L14"

    drop_collection(collection_name)
