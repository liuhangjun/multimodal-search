import os


def substitute_dict(file_path="/../assets/word_dict.txt"):
    substitute_dict = {}
    with open(os.path.dirname(os.path.realpath(__file__)) + file_path) as fd:
        for line in fd:
            parts = line.strip().split("|")
            if len(parts) != 2:
                print(f"[warning]解析词库: {line}格式不對")
                continue
            main_word, extend_str = parts
            substitute_list = [x.strip() for x in extend_str.split(",")]
            substitute_dict[main_word] = substitute_list
        print("替换词库为：", substitute_dict)

    return substitute_dict
    print(f"加载替换词库库结束，总共有替换词 {len(substitute_dict)}个")


if __name__ == "__main__":
    substitute_dict("/../assets/word_dict.txt")
