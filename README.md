# multimodal-search

#### 介绍
本工程是安装Chinese-clip大模型和milvus向量数据库，并实现文搜图的技术步骤

#### 软件架构
界面：gradio
模型：Chinsese clip
数据库: milvus

#### 安装教程
python 使用3.9的版本
1.  安装依赖包mac环境下使用requirements.txt, centOS8.5+T4卡的环境下使用requirements_T4_centOS8.5.txt
2.  运行setup.py进行打包，命令为：python setup.py sdist
3.  本工程要配合milvus向量数据库一起使用，创建表的脚本为：create_milvus_collection.py

#### 使用说明

1.  本项目主要提供两个功能
    python gradio_main.py 运行本工程
2.  输入文字搜索图片
3.  上传图片搜索图片

#### 参与贡献

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### 特性

使用 Readme_XXX.md 来支持不同的语言，例如 Readme_en.md, Readme_zh.md
Gitee 官方博客 blog.gitee.com
你可以 https://gitee.com/explore 这个地址来了解 Gitee 上的优秀开源项目
GVP 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
Gitee 官方提供的使用手册 https://gitee.com/help
Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 https://gitee.com/gitee-stars/