#!/bin/bash
export PYTHONPATH=.:$PYTHONPATH


mkdir -p dist/project/app
mkdir -p dist/project/data
mkdir -p dist/project/log

cp -rf app dist/project/
cp -rf data/*.py dist/project/data
cp -rf data/.gitignore dist/project/data

cp -rf .gitignore dist/project/
cp -rf *.sh dist/project/
cp -rf *.py dist/project/
cp -rf *.txt dist/project/
cp -rf *.md dist/project/
cp -rf requirements.txt dist/project/


current_date=$(date +"%Y-%m-%d_%H-%M-%S")
archive_name=multimodal-search-$current_date.zip

find ./dist/project/ -name .DS_Store -delete

zip -r dist/$archive_name dist/project/*

rm -rf dist/project/

