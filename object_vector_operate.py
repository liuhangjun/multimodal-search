import torch
from PIL import Image

import cn_clip.clip as clip
from cn_clip.clip import load_from_name, available_models

print("Available models:", available_models())
# Available models: ['ViT-B-16', 'ViT-L-14', 'ViT-L-14-336', 'ViT-H-14', 'RN50']

device = "cuda" if torch.cuda.is_available() else "cpu"
model, preprocess = load_from_name("ViT-H-14", device=device, download_root='./app/assets')
model.eval()


def get_object_vector(arg_image, arg_text):
    with torch.no_grad():
        text_features = model.encode_text(arg_text)
        # 对特征进行归一化，请使用归一化后的图文特征用于下游任务
        text_features /= text_features.norm(dim=-1, keepdim=True)
        emb = text_features.tolist()
        print("文向量为：", emb)

        image_features = model.encode_image(arg_image)
        # 对特征进行归一化，请使用归一化后的图文特征用于下游任务
        image_features /= image_features.norm(dim=-1, keepdim=True)
        emb = image_features.tolist()
        print("图向量为：", emb)

        logits_per_image, logits_per_text = model.get_similarity(arg_image, arg_text)
        probs = logits_per_image.softmax(dim=-1).cpu().numpy()
        print("相似度：", probs)

        print("--------------------------------")


if __name__ == '__main__':
    text_list = ["杯子", "水杯", "饭碗", "水壶"]
    text = clip.tokenize(text_list).to(device)

    print("相似度的文本数组:", text_list)

    image_name = "data/test1/杯子8.5M-3024x4032.jpg"
    print(image_name)
    image = preprocess(Image.open(image_name)).unsqueeze(0).to(device)
    get_object_vector(image, text)

    image_name = "data/test1/(杯子5.6M-3024x4032)output_srgb.jpg"
    print(image_name)
    image = preprocess(Image.open(image_name)).unsqueeze(0).to(device)
    get_object_vector(image, text)

    image_name = "data/test1/杯子4.5M-3024x4032.jpg"
    print(image_name)
    image = preprocess(Image.open(image_name)).unsqueeze(0).to(device)
    get_object_vector(image, text)

    image_name = "data/test1/杯子819K-1280x1707.jpg"
    print(image_name)
    image = preprocess(Image.open(image_name)).unsqueeze(0).to(device)
    get_object_vector(image, text)


