import os
import shutil


def move_heic_files(source_dir, destination_dir):
    # 确保目标目录存在
    if not os.path.exists(destination_dir):
        os.makedirs(destination_dir)

    # 获取源目录下的所有文件
    files = os.listdir(source_dir)

    # 遍历文件，将HEIC文件移动到目标目录
    for file in files:
        source_path = os.path.join(source_dir, file)

        # 检查文件是否为HEIC格式
        if file.lower().endswith(".heic"):
            destination_path = os.path.join(destination_dir, file)

            # 移动文件
            shutil.move(source_path, destination_path)
            print(f"Moved {file} to {destination_dir}")


if __name__ == "__main__":
    # 使用示例
    source_directory = "./test_phone"
    destination_directory = "./test_phone_heic"

    move_heic_files(source_directory, destination_directory)
