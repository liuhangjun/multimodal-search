import csv


def read_csv_lines(csv_file_path, start_line=1, end_line=10000):
    img_infos = {"image_ids": [], "image_paths": []}

    try:
        with open(csv_file_path, mode="r") as csv_file:
            csv_reader = csv.DictReader(csv_file)

            current_line = 0

            for row in csv_reader:
                current_line += 1

                if start_line <= current_line <= end_line:
                    img_infos["image_ids"].append(row["id"])
                    img_infos["image_paths"].append(row["path"])

                if current_line == end_line:
                    break

        print(img_infos)
    except FileNotFoundError:
        print(f"File not found: {csv_file_path}")
    except Exception as e:
        print(f"An error occurred: {e}")

    return img_infos


if __name__ == '__main__':
    csv_path = "csv/test_phone.csv"  # 请替换成你的CSV文件路径
    start_line = 1
    end_line = 10000

    read_csv_lines(csv_path, start_line, end_line)
